package br.com.rafaelchagasb.metadata.services;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.exceptions.StudentNotFoundException;
import br.com.rafaelchagasb.metadata.services.impl.StudentService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class StudentServiceTest {

	@Autowired
	private StudentService studentService;
	
	@Test
	public void testCreate_Success() {
		
		Student student = new Student("Robert", "Smith");
		
		studentService.create(student);
		
		Student saved = studentService.getById(student.getId()); 
		
		assertNotNull(saved.getId());
		assertEquals(saved.getFirstName(), student.getFirstName());
		assertEquals(saved.getLastName(), student.getLastName());
	}
	
	@Test
	public void testGetById_thenException() {

		StudentNotFoundException thrown = Assertions.assertThrows(StudentNotFoundException.class, () -> {
			studentService.getById(-999l); 
		});
		
		Assertions.assertEquals("Student doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testRemove_Success() {
		
		Student student = new Student("Robert", "Smith");
		
		studentService.create(student);
		
		studentService.remove(student.getId());
	}
	
	@Test
	public void testRemove_thenException() {

		StudentNotFoundException thrown = Assertions.assertThrows(StudentNotFoundException.class, () -> {
			studentService.remove(-999l);
		});
		
		Assertions.assertEquals("Student doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testUpdate_Success() {
		
		Student student = new Student("Robert", "Smith");
		
		studentService.create(student);
	
		studentService.update(student.getId(), new Student("Robert", "Williams"));
		
		Student updated = studentService.getById(student.getId());
		
		assertEquals("Robert", updated.getFirstName());
		
		assertEquals("Williams", updated.getLastName());
	}
	
	@Test
	public void testUpdate_thenException() {

		StudentNotFoundException thrown = Assertions.assertThrows(StudentNotFoundException.class, () -> {
			studentService.update(-999l, new Student("Robert", "Williams"));
		});
		
		Assertions.assertEquals("Student doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testListAll() {
		
		studentService.create(new Student("Rylan", "Hilll"));
		
		studentService.create(new Student("Malcolm", "Harber"));
		
		List<Student> students = studentService.listAll();
		
		List<String> names = students.stream().map(student -> student.getFirstName()).collect(Collectors.toList());
		
		assertThat(names, hasItems("Rylan"));
		
		assertThat(names, hasItems("Malcolm"));
	}
}
