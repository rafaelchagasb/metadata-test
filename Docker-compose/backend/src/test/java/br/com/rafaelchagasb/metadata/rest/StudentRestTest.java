package br.com.rafaelchagasb.metadata.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import br.com.rafaelchagasb.metadata.entities.Student;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class StudentRestTest extends BaseRestTest {

	@Test
	public void testGetStudents() {
		RestAssured.given().get("/students").then().statusCode(200);
	}
	
	@Test
	public void testCreateStudent() {
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();
		
		Student student = createStudent(firstName, lastName);
		
		assertNotNull(student.getId());
		assertEquals(firstName,  student.getFirstName());
		assertEquals(lastName,  student.getLastName());
	}
	
	@Test
	public void testGetStudent() {
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();
		
		Student student = createStudent(firstName, lastName);
		
		RestAssured.given().headers(_headers).accept("application/json").get("/students/" + student.getId()).then().statusCode(200);
	}
	
	@Test
	public void testGetStudent_NotFound() {
		RestAssured.given().headers(_headers).accept("application/json").get("/students/" + -999l).then().statusCode(404);
	}
	
	@Test
	public void testRemoveStudent() {
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();
		
		Student student = createStudent(firstName, lastName);
		
		RestAssured.given().headers(_headers).accept("application/json").delete("/students/" + student.getId()).then().statusCode(200);
	}
	
	private Student createStudent(String firstName, String lastName) {
		
		Student student = new Student();
		student.setFirstName(firstName);
		student.setLastName(lastName);
		String body = toJson(student);
		
		Response response = RestAssured.given().headers(_headers).accept("application/json").body(body).post("/students");
		response.then().statusCode(200);
		
		Student saved = (Student) toObject(response.asString(), Student.class);
		
		return saved;
	}
	
}
