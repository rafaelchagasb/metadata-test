package br.com.rafaelchagasb.metadata.services;

import static org.mockito.Mockito.doThrow;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.exceptions.CourseNotFoundException;
import br.com.rafaelchagasb.metadata.exceptions.LimitCourseExceededException;
import br.com.rafaelchagasb.metadata.exceptions.LimitStudentExceededException;
import br.com.rafaelchagasb.metadata.exceptions.StudentNotFoundException;
import br.com.rafaelchagasb.metadata.services.impl.CourseService;
import br.com.rafaelchagasb.metadata.services.impl.StudentService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RegistrationServiceMockTest {

	@Autowired
	private IRegistrationService registrationService;
	
	@MockBean
	@Qualifier("courseService")
	private CourseService courseService;
		
	@MockBean
	@Qualifier("studentService")
	private StudentService studentService;
	
	@Test
	public void testRegister_ThenException_CourseDontExist( ) {
		
		doThrow(CourseNotFoundException.class).when(courseService).getById(1L);
		
		Assertions.assertThrows(CourseNotFoundException.class, () -> {
			registrationService.register(1l, 1l);
		});
	}
	
	@Test
	public void testRegister_ThenException_StudentDontExist( ) {
			
		Long courseId = 1L;
			
		Long studentId = 1L;
		
		Mockito.when(courseService.getById(courseId)).thenReturn(new Course());
		
		doThrow(StudentNotFoundException.class).when(studentService).getById(studentId);
		
		Assertions.assertThrows(StudentNotFoundException.class, () -> {
			registrationService.register(courseId, studentId);
		});
	}
	
	@Test
	public void testRegister_ThenException_LimitMaxStudents( ) {

	    Long courseId = 1L;

	    Long studentId = 1L;
	    
	    Course course = createCourseWithFiftyStudents();

	    Mockito.when(courseService.getById(courseId)).thenReturn(course);

	    Mockito.when(studentService.getById(courseId)).thenReturn(new Student());

	    Assertions.assertThrows(LimitStudentExceededException.class, () -> {
	        registrationService.register(courseId, studentId);
	    });
	}
	
	@Test
	public void testRegister_ThenException_LimitMaxCourses( ) {
		
		Long courseId = 1L;
		
		Long studentId = 1L;
		
		Mockito.when(courseService.getById(courseId)).thenReturn(new Course());

	    Mockito.when(studentService.getById(courseId)).thenReturn(creatStudentWithFiveCourses());
		
		Assertions.assertThrows(LimitCourseExceededException.class, () -> {
			registrationService.register(courseId, studentId);
		});
	}
	
	@Test
	public void testRegister_Success() {
		
		Long courseId = 1L;
		
		Long studentId = 1L;
		
		Mockito.when(courseService.getById(courseId)).thenReturn(new Course());

	    Mockito.when(studentService.getById(courseId)).thenReturn(new Student());
		
	    registrationService.register(courseId, studentId);
	}
	
	private Course createCourseWithFiftyStudents() {
		Course course = new Course();
	    Set<Student> students = new HashSet<>();
	    for(int i = 0; i < 50; i++) {
	    	students.add(new Student());
	    }
	    course.setStudents(students);
	    return course;
	}
	
	private Student creatStudentWithFiveCourses() {
		Student student = new Student();
		Set<Course> courses = new HashSet<>();
		for(int i = 0; i < 5; i++) {
			courses.add(new Course());
		}
		student.setCourses(courses);
		return student;
	}
}
