package br.com.rafaelchagasb.metadata.rest;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.google.gson.Gson;

import io.restassured.RestAssured;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BaseRestTest {

	@LocalServerPort
	Integer port;
	
	protected Gson gson = new Gson();
	
	protected FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	
	Faker faker = new Faker();

	protected Map<String, String> _headers;
	
	@BeforeEach
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json");
		this._headers = headers;
	}
	
	protected String toJson(Object object) {
		return this.gson.toJson(object);
	}

	protected Object toObject(String data, Class classe) {
		return this.gson.fromJson(data, classe);
	}
	
}
