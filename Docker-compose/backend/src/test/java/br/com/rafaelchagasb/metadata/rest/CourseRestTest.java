package br.com.rafaelchagasb.metadata.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import br.com.rafaelchagasb.metadata.entities.Course;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class CourseRestTest extends BaseRestTest {
	
	@Test
	public void testGetCourses() {
		RestAssured.given().get("/courses").then().statusCode(200);
	}
	
	@Test
	public void testCreateCourse() {
		Course createCourse = createCourse("Medicine");
		
		assertNotNull(createCourse.getId());
		assertEquals("Medicine",  createCourse.getName());
	}
	
	@Test
	public void testGetCourse() {
		Course courseSaved =   createCourse("Medicine");
		
		RestAssured.given().headers(_headers).accept("application/json").get("/courses/" + courseSaved.getId()).then().statusCode(200);
	}
	
	@Test
	public void testRemoveCourse() {
		Course courseSaved = createCourse("Medicine");
		
		RestAssured.given().headers(_headers).accept("application/json").delete("/courses/" + courseSaved.getId()).then().statusCode(200);
	}
	
	@Test
	public void testUpdateCourse() {
		
		Course courseSaved = createCourse("Medicine");

		String bodyUpdated = toJson(new Course("Law"));
		
		RestAssured.given().headers(_headers).accept("application/json").body(bodyUpdated).put("/courses/" + courseSaved.getId()).then().statusCode(200);
	}

	private Course createCourse(String name) {
		
		Course course = new Course();
		course.setName(name);
		String body = toJson(course);
		
		Response response = RestAssured.given().headers(_headers).accept("application/json").body(body).post("/courses");
		response.then().statusCode(200);
		
		Course courseSaved = (Course) toObject(response.asString(), Course.class);
		
		return courseSaved;
	}
}
