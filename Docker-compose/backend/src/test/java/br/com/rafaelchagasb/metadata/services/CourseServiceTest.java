package br.com.rafaelchagasb.metadata.services;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.exceptions.CourseNotFoundException;
import br.com.rafaelchagasb.metadata.services.impl.CourseService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CourseServiceTest {

	@Autowired
	private CourseService courseService;
	
	@Test
	public void testCreate_Success() {
		
		Course course = new Course("Computer Science");
		
		courseService.create(course);
		
		Course saved = courseService.getById(course.getId()); 
		
		assertNotNull(saved.getId());
		assertEquals(saved.getName(), course.getName());
	}
	
	@Test
	public void testGetById_thenException() {

		CourseNotFoundException thrown = Assertions.assertThrows(CourseNotFoundException.class, () -> {
			courseService.getById(-999l);
		});
		
		Assertions.assertEquals("Course doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testRemove_Success() {
		
		Course course = new Course("Computer Science");
		
		courseService.create(course);
		
		courseService.remove(course.getId());
	}
	
	@Test
	public void testRemove_thenException() {

		CourseNotFoundException thrown = Assertions.assertThrows(CourseNotFoundException.class, () -> {
			courseService.remove(-999l);
		});
		
		Assertions.assertEquals("Course doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testUpdate_Success() {
		
		Course saved = courseService.create(new Course("Computer Science"));
	
		Course updated = new Course("Law");
		
		courseService.update(saved.getId(), updated);
		
		assertEquals("Law", courseService.getById(saved.getId()).getName());
	}
	
	@Test
	public void testUpdate_thenException() {

		CourseNotFoundException thrown = Assertions.assertThrows(CourseNotFoundException.class, () -> {
			courseService.update(-999l, new Course("Law"));
		});
		
		Assertions.assertEquals("Course doesn't found", thrown.getMessage());
	}
	
	@Test
	public void testListAll() {
		
		courseService.create(new Course("Medicine"));
		
		courseService.create(new Course("Business"));
		
		List<Course> courses = courseService.listAll();
		
		List<String> names = courses.stream().map(course -> course.getName()).collect(Collectors.toList());
		
		assertThat(names, hasItems("Medicine"));
		
		assertThat(names, hasItems("Business"));
	}
	
}
