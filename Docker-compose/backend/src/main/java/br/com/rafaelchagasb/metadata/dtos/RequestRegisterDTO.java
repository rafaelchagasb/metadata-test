package br.com.rafaelchagasb.metadata.dtos;

import org.springframework.lang.NonNull;

public class RequestRegisterDTO {

	@NonNull
	private Long studentId;
	
	@NonNull
	private Long courseId;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	
}
