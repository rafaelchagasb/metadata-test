package br.com.rafaelchagasb.metadata.responses;

import java.time.LocalDateTime;

import org.springframework.lang.NonNull;

public class MessageErrorResponse {

	@NonNull
	private String message;
	
	@NonNull
	private LocalDateTime timestamp;

	public MessageErrorResponse(String message) {
		this.message = message;
		this.timestamp = LocalDateTime.now();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	
}
