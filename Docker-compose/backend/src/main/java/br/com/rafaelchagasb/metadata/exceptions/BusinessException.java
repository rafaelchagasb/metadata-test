package br.com.rafaelchagasb.metadata.exceptions;

public class BusinessException extends RuntimeException{

	private static final long serialVersionUID = 4335500869774245475L;

	public BusinessException(String message) {
		super(message);
	}
}
