package br.com.rafaelchagasb.metadata.exceptions;

public class LimitCourseExceededException extends BusinessException{

	private static final long serialVersionUID = -4716026927972108722L;
	
	public LimitCourseExceededException() {
		super("This student already has 5 courses registered");
	}
}
