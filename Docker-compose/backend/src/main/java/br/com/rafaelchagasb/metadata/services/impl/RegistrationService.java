package br.com.rafaelchagasb.metadata.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rafaelchagasb.metadata.dtos.CourseWithStudents;
import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.exceptions.LimitCourseExceededException;
import br.com.rafaelchagasb.metadata.exceptions.LimitStudentExceededException;
import br.com.rafaelchagasb.metadata.services.IRegistrationService;

@Service
public class RegistrationService implements IRegistrationService {

	@Autowired
	private CourseService courseService;
	
	@Autowired
	private StudentService studentService;
	
	public static final int LIMIT_STUDENTS_IN_COURSE = 50;
		
	public static final int LIMIT_COURSE_PER_STUDENT = 5;
	
	@Override
	public void register(Long courseId, Long studentId) {
		
		Course course = courseService.getById(courseId);
		
		Student student = studentService.getById(studentId);
		
		if(course.getStudents().size() >= LIMIT_STUDENTS_IN_COURSE) {
			throw new LimitStudentExceededException();
		}
		
		if(student.getCourses().size() >= LIMIT_COURSE_PER_STUDENT) {
			throw new LimitCourseExceededException();
		}
		
		course.getStudents().add(student);
		
		courseService.update(courseId, course);
	}

	@Override
	public List<CourseWithStudents> getCoursesWithStudents() {
		return courseService.listAll().stream().map(x -> new CourseWithStudents(x)).collect(Collectors.toList());
	}
}
