package br.com.rafaelchagasb.metadata.dtos;

import java.util.HashSet;
import java.util.Set;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;

public class CourseWithStudents {

	public Long id;

	private String name;

	private Set<Student> students = new HashSet<>();
	
	public CourseWithStudents(Course course) {
		this.id = course.getId();
		this.name = course.getName();
		this.students = course.getStudents();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	
}
