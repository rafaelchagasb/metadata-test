package br.com.rafaelchagasb.metadata.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.exceptions.CourseNotFoundException;
import br.com.rafaelchagasb.metadata.repositories.CourseRepository;
import br.com.rafaelchagasb.metadata.services.ICourseService;

@Service
public class CourseService implements ICourseService {

	@Autowired
	private CourseRepository courseRepository;
	
	@Override
	public List<Course> listAll() {
		return (List<Course>) courseRepository.findAll();
	}

	@Override
	public Course create(Course course) {
		return courseRepository.save(course);
	}

	@Override
	@Transactional 
	public void remove(Long courseId) {
		
		checkExistCourse(courseId);
		
		Course course = getById(courseId);
		
		course.getStudents().removeAll(new HashSet<Student>());
		
		courseRepository.save(course);

		courseRepository.deleteById(courseId);
	}

	@Override
	public Course update(Long courseId, Course course) {
		
		checkExistCourse(courseId);
		
		course.setId(courseId);
		
		return courseRepository.save(course);
	}

	@Override
	public Course getById(Long courseId) {

		Optional<Course> course = courseRepository.findById(courseId);
		
		if(!course.isPresent()) {
			throw new CourseNotFoundException();
		}
		
		return course.get();
	}

	@Override
	public Collection<Student> getStudentsOfCourse(Long courseId) {
		return getById(courseId).getStudents();
	}
	

	/**
	 * Check if course exist.
	 * 
	 * Case course dont exist will throw CourseNotFoundException
	 * 
	 * @param courseId Identifier of course
	 * 
	 */
	@Override
	public void checkExistCourse(Long courseId) {
		if (!courseRepository.existsById(courseId)) {
			throw new CourseNotFoundException();
		}
	}

}
