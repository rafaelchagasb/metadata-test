package br.com.rafaelchagasb.metadata.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.exceptions.StudentNotFoundException;
import br.com.rafaelchagasb.metadata.repositories.StudentRepository;
import br.com.rafaelchagasb.metadata.services.IStudentService;

@Service
public class StudentService implements IStudentService{
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Override
	public List<Student> listAll() {
		return (List<Student>) studentRepository.findAll();
	}

	@Override
	public Student create(Student student) {
		return studentRepository.save(student);
	}

	@Override
	@Transactional
	public void remove(Long studentId) {
		
		checkExistStudent(studentId);
		
		Student student = getById(studentId);
		
		student.getCourses().removeAll(new HashSet<Course>());
		
		studentRepository.save(student);
		
		studentRepository.deleteById(studentId);
	}

	@Override
	public Student update(Long studentId, Student student) {

		checkExistStudent(studentId);
		
		student.setId(studentId);
		
		return studentRepository.save(student);
	}
	

	@Override
	public Student getById(Long studentId) {
		
		Optional<Student> student = studentRepository.findById(studentId);
		
		if(!student.isPresent()) {
			throw new StudentNotFoundException();
		}
		
		return student.get();
	}
	
	@Override
	public Collection<Course> getCoursesOfStudent(Long studentId) {
		return getById(studentId).getCourses();
	}
	
	/**
	 * Check if student exist. 
	 * 
	 * Case student dont exist will throw StudentNotFoundException
	 * 
	 * @param studentId Identifier of student
	 * 
	 */
	@Override
	public void checkExistStudent(Long studentId) {
		if(!studentRepository.existsById(studentId)) {
			throw new StudentNotFoundException();
		}
	}

}
