package br.com.rafaelchagasb.metadata.exceptions;

public class LimitStudentExceededException extends BusinessException{

	private static final long serialVersionUID = -1344229090864514398L;
	
	public LimitStudentExceededException() {
		super("A course has 50 students maximum");
	}
}
