package br.com.rafaelchagasb.metadata.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rafaelchagasb.metadata.entities.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{

}
