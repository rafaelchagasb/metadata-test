package br.com.rafaelchagasb.metadata.exceptions;

public class EntityNotFoundException extends RuntimeException{

	private static final long serialVersionUID = -4706479477377169231L;
	
	public EntityNotFoundException(String message) {
		super(message);
	}
}
