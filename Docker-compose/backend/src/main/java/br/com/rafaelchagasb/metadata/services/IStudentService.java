package br.com.rafaelchagasb.metadata.services;

import java.util.Collection;
import java.util.List;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;

public interface IStudentService {

	List<Student> listAll();

	Student create(Student student);

	void remove(Long studentId);

	Student update(Long studentId, Student student);

	Student getById(Long studentId);

	Collection<Course> getCoursesOfStudent(Long studentId);
	
	void checkExistStudent(Long studentId);
	
}
