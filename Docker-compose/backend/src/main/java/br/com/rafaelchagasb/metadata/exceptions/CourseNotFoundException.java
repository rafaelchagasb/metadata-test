package br.com.rafaelchagasb.metadata.exceptions;

public class CourseNotFoundException extends EntityNotFoundException {

	private static final long serialVersionUID = -3687912505589875483L;
	
	public CourseNotFoundException() {
		super("Course doesn't found");
	}
}
