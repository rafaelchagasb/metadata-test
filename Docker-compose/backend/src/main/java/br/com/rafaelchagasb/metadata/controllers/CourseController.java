package br.com.rafaelchagasb.metadata.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.services.ICourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/courses")
@Api(value = "Courses", tags = "courses")
public class CourseController {

	@Autowired
	private ICourseService service;
	
	@ApiOperation(value = "Return the list of courses")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Return the list of courses"),
	})
	@GetMapping
	public List<Course> getAll() {
		return (List<Course>) service.listAll();
	}

	@ApiOperation(value = "Create course")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Course saved"),
	})
	@PostMapping	
	public Course create(@Valid @RequestBody Course course) {
        return service.create(course);
    }
	
	@ApiOperation(value = "Remove course")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Course removed"),
	    @ApiResponse(code = 400, message = "Course doesn't found"),
	})
	@DeleteMapping("/{id}")	
	public void remove(@PathVariable("id") Long courseId) {
        service.remove(courseId);
    }
	
	@ApiOperation(value = "Update course")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Course updated"),
	    @ApiResponse(code = 400, message = "Course doesn't found"),
	})
	@PutMapping("/{id}")	
	public Course update(@PathVariable("id") Long courseId, @Valid @RequestBody Course course) {
        return service.update(courseId, course);
    }
	
	@ApiOperation(value = "Return course")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Course returned"),
	    @ApiResponse(code = 400, message = "Course doesn't found"),
	})
	@GetMapping("/{id}")	
	public Course getById(@PathVariable("id") Long courseId) {
        return service.getById(courseId);
    }
	
	@ApiOperation(value = "Returns list of students")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Returns the list of students for a specific course"),
	})
	@GetMapping("/{id}/students")	
	public Collection<Student> getStudents(@PathVariable("id") Long courseId) {
        return service.getStudentsOfCourse(courseId);
    }
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}
