package br.com.rafaelchagasb.metadata.services;

import java.util.Collection;
import java.util.List;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;

public interface ICourseService {

	List<Course> listAll();

	Course create(Course course);

	void remove(Long courseId);

	Course update(Long courseId, Course course);

	Course getById(Long courseId);

	Collection<Student> getStudentsOfCourse(Long courseId);
	
	void checkExistCourse(Long courseId);

}
