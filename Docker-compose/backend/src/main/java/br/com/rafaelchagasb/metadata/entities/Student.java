package br.com.rafaelchagasb.metadata.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Student {

	@ApiModelProperty(value = "Student identifier")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@ApiModelProperty(value = "Student's first name")
	@NonNull
	@Column(nullable = false)
    @NotBlank(message = "firstName is mandatory")
	private String firstName;

	@ApiModelProperty(value = "Student's last name")
	@NonNull
	@Column(nullable = false)
	@NotBlank(message = "lastName is mandatory")
	private String lastName;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "students")
    private Set<Course> courses = new HashSet<>();

	public Student() {
	}
	
	public Student(Long id) {
		this.id = id;
	}
	
	public Student(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
	
	public void removeCourse(Course course) {
        this.courses.remove(course);
        course.getStudents().remove(this);
    }

}
