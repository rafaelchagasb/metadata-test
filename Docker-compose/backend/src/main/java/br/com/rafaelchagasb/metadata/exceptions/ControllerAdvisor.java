package br.com.rafaelchagasb.metadata.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.rafaelchagasb.metadata.responses.MessageErrorResponse;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<MessageErrorResponse> handleEntityNotFound(EntityNotFoundException ex, WebRequest request) {
        return new ResponseEntity<>(new MessageErrorResponse(ex.getMessage()), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(LimitCourseExceededException.class)
    public ResponseEntity<MessageErrorResponse> handleLimitCourses(LimitCourseExceededException ex, WebRequest request) {
        return new ResponseEntity<>(new MessageErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(LimitStudentExceededException.class)
    public ResponseEntity<MessageErrorResponse> handleLimitStudentsPerCourse(LimitStudentExceededException ex, WebRequest request) {
        return new ResponseEntity<>(new MessageErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
