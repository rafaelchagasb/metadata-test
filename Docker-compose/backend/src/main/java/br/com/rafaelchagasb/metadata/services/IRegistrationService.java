package br.com.rafaelchagasb.metadata.services;

import java.util.List;

import br.com.rafaelchagasb.metadata.dtos.CourseWithStudents;

public interface IRegistrationService {

	void register(Long courseId, Long studentId);
	
	List<CourseWithStudents> getCoursesWithStudents();
	
}
