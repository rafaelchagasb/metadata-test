package br.com.rafaelchagasb.metadata.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rafaelchagasb.metadata.entities.Course;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long>{

}
