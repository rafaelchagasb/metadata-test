package br.com.rafaelchagasb.metadata.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rafaelchagasb.metadata.dtos.CourseWithStudents;
import br.com.rafaelchagasb.metadata.dtos.RequestRegisterDTO;
import br.com.rafaelchagasb.metadata.services.IRegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping
@Api(value = "Registration", tags = "registration")
public class RegistrationController {
	
	@Autowired
	private IRegistrationService registrationService;
	
	@ApiOperation(value = "Register to courses")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "register successfully"),
	})
	@PostMapping("/register")		
	public void register(@Valid @RequestBody RequestRegisterDTO dto) {
		registrationService.register(dto.getCourseId(), dto.getStudentId());
    }
	
	@ApiOperation(value = "List all courses with all students")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "List courses returned"),
	    @ApiResponse(code = 400, message = "This student already has 5 courses registered"),
	    @ApiResponse(code = 400, message = "A course has 50 students maximum"),
	})
	@GetMapping("/overview")
	public List<CourseWithStudents> getCoursesWithStudents() {
		return (List<CourseWithStudents>) registrationService.getCoursesWithStudents();
	}
}
