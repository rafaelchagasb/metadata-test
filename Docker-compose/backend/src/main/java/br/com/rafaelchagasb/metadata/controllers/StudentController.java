package br.com.rafaelchagasb.metadata.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.rafaelchagasb.metadata.entities.Course;
import br.com.rafaelchagasb.metadata.entities.Student;
import br.com.rafaelchagasb.metadata.services.IStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/students")
@Api(value = "Students", tags = "students")
public class StudentController {

	@Autowired
	private IStudentService service;

	@ApiOperation(value = "Return the list of students")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Return the list of students"),
	})
	@GetMapping
	public List<Student> getAllStudents() {
		return (List<Student>) service.listAll();
	}

	@ApiOperation(value = "Create student")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Student saved"),
	})
	@PostMapping
	public Student create(@Valid @RequestBody Student student) {
		return service.create(student);
	}

	@ApiOperation(value = "Remove student")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Student removed"),
	    @ApiResponse(code = 400, message = "Student doesn't found"),
	})
	@DeleteMapping("/{id}")
	public void remove(@PathVariable("id") Long studentId) {
		service.remove(studentId);
	}

	@ApiOperation(value = "Update student")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Student updated"),
	    @ApiResponse(code = 400, message = "Student doesn't found"),
	})
	@PutMapping("/{id}")
	public Student update(@PathVariable("id") Long studentId, @Valid @RequestBody Student student) {
		return service.update(studentId, student);
	}
	
	@ApiOperation(value = "Return student")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Student returned"),
	    @ApiResponse(code = 400, message = "Student doesn't found"),
	})
	@GetMapping("/{id}")
	public Student getById(@PathVariable("id") Long studentId) {
		return service.getById(studentId);
	}

	@ApiOperation(value = "Returns list of courses")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Returns the list of courses for a specific student"),
	})
	@GetMapping("/{id}/courses")
	public Collection<Course> listCourses(@PathVariable("id") Long studentId) {
		return service.getCoursesOfStudent(studentId);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}
