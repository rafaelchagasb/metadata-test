package br.com.rafaelchagasb.metadata.exceptions;

public class StudentNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 4573472502393699018L;
	
	public StudentNotFoundException() {
		super("Student doesn't found");
	}
}
