# Senior Software Engineer - test

# Instructions

## How to setup

###  Compile and Up

```shell
git clone https://rafaelchagasb@bitbucket.org/rafaelchagasb/metadata-test.git
cd metadata-test/Docker-compose
docker-compose up -d
```

### Open Swagger

Open in browser: swagger-ui.html

File location: ${ROOT_FOLDER_REPOSITORY}/swagger-ui.html

## Collection for Postman

Import collection in Postman

Postman > Import > [Collection](https://bitbucket.org/rafaelchagasb/metadata-test/src/master/Collection%20Postman/Metadata.postman_collection.json)

File location: ${ROOT_FOLDER_REPOSITORY}/Collection%20Postman/Metadata.postman_collection.json

## Endpoints and payloads

Host: http://localhost:8080

Example: http://localhost:8080/students

### Students

 ROUTE                          |     HTTP(Verb)        |      Description                          | 
------------------------------- | --------------------- | ----------------------------------------- | 
/students                       |       GET             | List all students       			        | 
/students                       |       POST            | Create student                            | 
/students/:student_id           |       GET             | Get student per id                        | 
/students/:student_id/          |       PUT             | Update per id                             |    
/students/:student_id/          |       DELETE          | Delete per id                             |
/students/:student_id/courses   |       GET 		    | List all courses for a specific student   |

### Courses

 ROUTE                          |     HTTP(Verb)        |      Description                          | 
------------------------------- | --------------------- | ----------------------------------------- | 
/courses                        |       GET             | List all coursese       			        | 
/courses                        |       POST            | Create course                             | 
/courses/:course_id             |       GET             | Get course per ID                         | 
/courses/:course_id/            |       PUT             | Update per id                             |    
/courses/:course_id/            |       DELETE          | Delete per id                             |
/courses/:course_id/students    |       GET 		    | List all students for a specific course   |

### Registration and Overview


 ROUTE                          |     HTTP(Verb)        |      Description                          			| 
------------------------------- | --------------------- | ----------------------------------------------------- | 
/register                       |       POST            | List all coursese       			        			| 
/overview                       |       GET             | List all relationships between students and courses 	| 


# Technology stack used:
- Java 11
- Maven
- Spring Boot
- Docker (docker-compose)
- JUnit 5
- RestAssured
- Mockito
- MySQL